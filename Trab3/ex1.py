def numero_primo():
    numero = int(input('Insira o numero que deseja saber se é primo?\n'))
    ndiv = 0
    divisores = []
    for x in range(1,numero+1):
        if numero%x == 0:
            ndiv += 1
            divisores.append(x)
    if ndiv > 2:
        print(divisores)
        print('numero nao primo')
    else:
        print(f'numero {numero} é primo pois só é divisivel por {divisores[0]} e {divisores[1]}')
        #print(divisores)
numero_primo()
