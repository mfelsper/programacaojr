from math import pow
from math import sqrt


def distancia_pontos():
    ponto_a = []
    ponto_b = []
    for i in range(0, 2):
        a = int(input('\nDigite as cordenadas do ponto a: \n'))
        ponto_a.append(a)
    for i in range(0, 2):
        b = int(input('\nDigite as cordenadas do ponto b: \n'))
        ponto_b.append(b)
    # print(ponto_a,ponto_b)
    distancia = sqrt(pow(ponto_a[0] - ponto_b[0], 2) + pow(ponto_a[1] - ponto_b[1], 2))
    print(distancia)

while True:
    pressed = input('\nDeseja calcular a distância entre dois pontos? y/n\n').upper()
    if pressed == 'N':
        break
    elif pressed == 'Y':
        distancia_pontos()
        continue
    else:
        print('\nOpção errada, prima "y" para continuar ou "n" para sair.\n')



