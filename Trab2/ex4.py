def circunferencia():
    pi = 3.1415
    raio = float(input('Qual o raio da circunferência?\n'))
    perimetro = 2*raio*pi
    area = pi*raio**2
    print(f'O perimetro desta circunferencia é {perimetro} e a area é {area}')
circunferencia()
