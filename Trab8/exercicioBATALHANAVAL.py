import json

BARCOS = [["Aircraft Carrier", 5, "@  ", "@* ", False],
          ["Battleship", 4, "#  ", "#* ", False],
          ["Submarine", 3, "+  ", "+* ", False],
          ["Cruiser", 3, "?  ", "?* ", False],
          ["Destroyer", 2, "!  ", "!* ", False]]


def imprime_mensagem_abertura():
    print("*********************************")
    print("***Bem vindo ao jogo Batalha Naval!***")
    print("*********************************")


def pede_cordenadas():
    while True:
        try:
            cord = float(input("Inserir Coordenadas: "))
            if cord <= 10.0:
                linha = int(cord)
                coluna = int(cord * 10) - (int(cord) * 10)
                # print(linha, coluna)
                return linha, coluna
            else:
                print("Cordenada inválida")
        except ValueError as vl:
            print(vl)


def pede_orientacao():
    while True:
        escolha = str(input("Inserir Orientacao:(H)orizontal/(V)ertical")).upper()
        if escolha == "H" or escolha == "V":
            return escolha
        else:
            print("Opção não valida, escolha V ou H")


def desenha_tabuleiro(tabuleiro, jogador):
    print("\n            TABULEIRO do Jogador ", jogador, "\n")
    print("    0↓  ", end="")
    for i in range(1, 10):
        print(f"{i}↓  ", end="")
    print("\n    __________________________________________________")
    for i in range(0, 10):
        print(f"{i}> [", end="")
        for j in range(0, 10):
            if j == 9:
                print(tabuleiro[i][j], "]")
            else:
                print(tabuleiro[i][j], "|", end="")
    print("    __________________________________________________")


def coloca_barcos(jogador):
    tabuleiro = [["   " for c in range(10)] for d in range(10)]
    i = 0
    while i < len(BARCOS):
        print(i)
        desenha_tabuleiro(tabuleiro, jogador)
        print("Colocar", BARCOS[i][0])
        orientacao = pede_orientacao()
        anchor = 0
        cord = pede_cordenadas()
        linha = cord[0]
        coluna = cord[1]
        tamanho = BARCOS[i][1]

        if orientacao == "H":
            while coluna + tamanho > 10:
                print("Nao cabe")
                cord = pede_cordenadas()
                linha = cord[0]
                coluna = cord[1]
                tamanho = BARCOS[i][1]

            for j in range(0, tamanho):
                if tabuleiro[linha][coluna + j] == "   ":
                    anchor += 1
                    if anchor == tamanho:
                        for j in range(tamanho):
                            tabuleiro[linha][coluna + j] = BARCOS[i][2]
                        i += 1
                else:
                    print("Ja tem um barco nessas coordenadas!")
                    break

        elif orientacao == "V":
            while linha + tamanho > 10:
                print("Nao cabe")
                cord = pede_cordenadas()
                linha = cord[0]
                coluna = cord[1]
                tamanho = BARCOS[i][1]

            for j in range(0, tamanho):
                if tabuleiro[linha + j][coluna] == "   ":
                    anchor += 1
                    if anchor == tamanho:
                        for j in range(tamanho):
                            tabuleiro[linha + j][coluna] = BARCOS[i][2]
                        i += 1
                else:
                    print("Ja tem um barco nessas coordenadas!")
                    break

    return tabuleiro


def ataca_tabuleiro(tabuleiro, tabuleiro_revelado):
    while True:
        cord = pede_cordenadas()
        linha = cord[0]
        coluna = cord[1]
        if tabuleiro[linha][coluna] == "   ":
            tabuleiro_revelado[linha][coluna] = " * "
            tabuleiro[linha][coluna] = " * "
            return tabuleiro_revelado
        for i in range(len(BARCOS)):
            if tabuleiro[linha][coluna] == BARCOS[i][2]:
                tabuleiro[linha][coluna] = BARCOS[i][3]
                tabuleiro_revelado[linha][coluna] = BARCOS[i][3]
                return tabuleiro_revelado
            elif tabuleiro[linha][coluna] == BARCOS[i][3]:
                print("Já atacou esta posicao e acertou!")
                break
            elif tabuleiro[linha][coluna] == " * ":
                print("Já atacou esta posicao e errou!")
                break


def verifica_barcos_totalmente_explodidos(tabuleiro):
    print("Verificando")
    # VERTICAL
    for i in range(len(BARCOS)):
        anchor = 1
        for j in range(len(tabuleiro)):
            for k in range(len(tabuleiro[0])):
                tamanho = BARCOS[i][1]
                if j + 1 > 9:
                    break
                if tabuleiro[j][k] == BARCOS[i][3] and tabuleiro[j + 1][k] == BARCOS[i][3]:
                    anchor += 1
                    if anchor == tamanho:
                        BARCOS[i][4] = True
                        print(f"PUM! {BARCOS[i][0]} ao fundo!")
        # HORIZONTAL
        for j in range(len(tabuleiro)):
            anchor_dois = 1
            for k in range(len(tabuleiro[0])):
                tamanho = BARCOS[i][1]
                if k + 1 > 9:
                    break
                if tabuleiro[j][k] == BARCOS[i][3] and tabuleiro[j][k + 1] == BARCOS[i][3]:
                    anchor_dois += 1
                    if anchor_dois == tamanho:
                        BARCOS[i][4] = True
                        print(f"PUM! {BARCOS[i][0]} ao fundo!")


def verifica_ganhou(tabuleiro_revelado, jogador, jogador_dois):
    anchor = 0
    for i in range(len(BARCOS)):
        if BARCOS[i][4]:
            anchor += 1
            if anchor == 5:
                print("\n#######################################################\nPARABENS!!!!\nJogador ", jogador,
                      " afundou todos os barcos do Jogador ", jogador_dois + " !!")
                desenha_tabuleiro(tabuleiro_revelado, jogador)
                return True
    return False


def game(tabuleiro_revelado_um, tabuleiro_um_com_barcos,
         tabuleiro_revelado_dois, tabuleiro_dois_com_barcos, jogada):
    global tabuleiro_revelado_do_jogador_um, tabuleiro_revelado_do_jogador_dois
    print(
        "\n#######################################################\nPassando a Faze de Ataque (*)\n#######################################################")
    jogador_um_ganhou = False
    jogador_dois_ganhou = False
    try:
        while jogador_um_ganhou == False or jogador_dois_ganhou == False:
            if jogada % 2 + 1 == 1:
                print(
                    "\n#######################################################\nJogador 2 atacando os barcos do tabuleiro do Jogador 1:\n")
                tabuleiro_revelado_do_jogador_um = ataca_tabuleiro(tabuleiro_um_com_barcos, tabuleiro_revelado_um)
                desenha_tabuleiro(tabuleiro_revelado_do_jogador_um, 1)
                verifica_barcos_totalmente_explodidos(tabuleiro_revelado_do_jogador_um)
                jogador_dois_ganhou = verifica_ganhou(tabuleiro_revelado_do_jogador_um, 1, 2)
                if jogador_dois_ganhou:
                    salva_ficheiro(jogada, [], [], [], [])
                    break
            else:
                print(
                    "\n#######################################################\nJogador 1 atacando os barcos do tabuleiro do Jogador 2:\n")
                tabuleiro_revelado_do_jogador_dois = ataca_tabuleiro(tabuleiro_dois_com_barcos, tabuleiro_revelado_dois)
                desenha_tabuleiro(tabuleiro_revelado_do_jogador_dois, 2)
                verifica_barcos_totalmente_explodidos(tabuleiro_revelado_do_jogador_dois)
                jogador_um_ganhou = verifica_ganhou(tabuleiro_revelado_do_jogador_dois, 2, 1)
                if jogador_um_ganhou:
                    salva_ficheiro(jogada, [], [], [], [])
                    break
            jogada += 1
    except KeyboardInterrupt as kb:
        print(kb)
        salva_ficheiro(jogada, tabuleiro_um_com_barcos, tabuleiro_dois_com_barcos,
                       tabuleiro_revelado_do_jogador_um, tabuleiro_revelado_do_jogador_dois, True)
        exit()


def salva_ficheiro(jogada, tabuleiro_um_com_barcos, tabuleiro_dois_com_barcos,
                   tabuleiro_revelado_do_jogador_um, tabuleiro_revelado_do_jogador_dois, interruptor=False):
    load = carrega_ficheiro()

    if not interruptor:  # interruptor == False:
        load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_UM_BARCOS"] = []
        load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_DOIS_BARCOS"] = []
        load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_REVELADO_UM"] = []
        load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_REVELADO_DOIS"] = []
        if jogada % 2 + 1 == 1:  # quem ganhou foi o 1
            load["BATALHANAVAL"]["MULTIPLAYER"]["VITORIAS"]["JOGADOR_1"] += 1
        else:
            load["BATALHANAVAL"]["MULTIPLAYER"]["VITORIAS"]["JOGADOR_2"] += 1
    else:
        load["BATALHANAVAL"]["MULTIPLAYER"]["JOGADA"] = jogada
        load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_UM_BARCOS"] = tabuleiro_um_com_barcos
        load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_DOIS_BARCOS"] = tabuleiro_dois_com_barcos
        load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_REVELADO_UM"] = tabuleiro_revelado_do_jogador_um
        load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_REVELADO_DOIS"] = tabuleiro_revelado_do_jogador_dois
    with open('savebtnaval.json', 'w') as jsonfile:
        json.dump(load, jsonfile)


def carrega_ficheiro():
    with open('savebtnaval.json', 'r') as jsonfile:
        load = json.load(jsonfile)
    return load


def main():
    while True:
        continuar = input("##########################\n0. Sair \n1. Jogar\nEscolha a opção:")
        load = carrega_ficheiro()
        if continuar == "1":
            option = input('\nNovo Jogo?[1]\nContinuar Antigo?[2]\nMostrar Pontuação[3]')
            imprime_mensagem_abertura()
            if option == '1':
                print(
                    "\n#######################################################\nJogador 1 escondendo os barcos em seu tabuleiro:\n")
                tabuleiro_um_com_barcos = coloca_barcos(1)
                tabuleiro_vazio_um = [["   " for c in range(10)] for d in range(10)]
                print(
                    "\n#######################################################\nJogador 2 escondendo os barcos em seu tabuleiro:\n")
                tabuleiro_dois_com_barcos = coloca_barcos(2)
                tabuleiro_vazio_dois = [["   " for c in range(10)] for d in range(10)]

                game(tabuleiro_vazio_um, tabuleiro_um_com_barcos,
                     tabuleiro_vazio_dois, tabuleiro_dois_com_barcos, 0)
                continue
            elif option == '2':
                jogada = load["BATALHANAVAL"]["MULTIPLAYER"]["JOGADA"]
                tabuleiro_revelado_do_jogador_um = load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_REVELADO_UM"]
                tabuleiro_revelado_do_jogador_dois = load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_REVELADO_DOIS"]
                if load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_UM_BARCOS"] == []:
                    tabuleiro_um_com_barcos = coloca_barcos(1)
                else:
                    tabuleiro_um_com_barcos = load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_UM_BARCOS"]
                if load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_DOIS_BARCOS"] == []:
                    tabuleiro_dois_com_barcos = coloca_barcos(2)
                else:
                    tabuleiro_dois_com_barcos = load["BATALHANAVAL"]["MULTIPLAYER"]["TABULEIRO_DOIS_BARCOS"]

                game(tabuleiro_revelado_do_jogador_um, tabuleiro_um_com_barcos,
                     tabuleiro_revelado_do_jogador_dois, tabuleiro_dois_com_barcos, jogada)
                continue
            elif continuar == "0":
                print("Saiu do jogo")
                exit()
        else:
            print("Opção inválida, tente outra vez.")
            continue


if (__name__ == '__main__'):
    main()
