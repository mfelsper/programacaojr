import random
import json


def imprime_mensagem_abertura():
    print("*********************************")
    print("***Bem vindo ao jogo da Forca!***")
    print("*********************************")


def carrega_palavra_secreta():
    # palavras = []
    # with open("palavras.txt", "r", encoding="utf-8") as arquivo:
    #     for linha in arquivo:
    #         linha = linha.strip()
    #         palavras.append(linha)
    #
    # numero = random.randrange(0, len(palavras))
    # palavra_secreta = palavras[numero].upper()
    palavra_secreta = input('Qual palavra secreta quer?').upper()

    return palavra_secreta


def inicializa_letras_acertadas(palavra):
    return ["_" for letra in palavra]


def pede_chute():
    chute = input("Qual letra? ")
    chute = chute.strip().upper()
    return chute


def marca_chute_correto(chute, letras_acertadas, palavra_secreta):
    index = 0
    for letra in palavra_secreta:
        if (chute == letra):
            letras_acertadas[index] = letra
        index += 1


def imprime_mensagem_vencedor():
    print("Parabéns, você ganhou!")
    print("       ___________      ")
    print("      '._==_==_=_.'     ")
    print("      .-\\:      /-.    ")
    print("     | (|:.     |) |    ")
    print("      '-|:.     |-'     ")
    print("        \\::.    /      ")
    print("         '::. .'        ")
    print("           ) (          ")
    print("         _.' '._        ")
    print("        '-------'       ")


def imprime_mensagem_perdedor(palavra_secreta):
    print("Puxa, você foi enforcado!")
    print("A palavra era {}".format(palavra_secreta))
    print("    _______________         ")
    print("   /               \       ")
    print("  /                 \      ")
    print("//                   \/\  ")
    print("\|   XXXX     XXXX   | /   ")
    print(" |   XXXX     XXXX   |/     ")
    print(" |   XXX       XXX   |      ")
    print(" |                   |      ")
    print(" \__      XXX      __/     ")
    print("   |\     XXX     /|       ")
    print("   | |           | |        ")
    print("   | I I I I I I I |        ")
    print("   |  I I I I I I  |        ")
    print("   \_             _/       ")
    print("     \_         _/         ")
    print("       \_______/           ")


def desenha_forca(erros):
    print("  _______     ")
    print(" |/      |    ")

    if (erros == 1):
        print(" |      (_)   ")
        print(" |            ")
        print(" |            ")
        print(" |            ")

    if (erros == 2):
        print(" |      (_)   ")
        print(" |      \     ")
        print(" |            ")
        print(" |            ")

    if (erros == 3):
        print(" |      (_)   ")
        print(" |      \|    ")
        print(" |            ")
        print(" |            ")

    if (erros == 4):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |            ")
        print(" |            ")

    if (erros == 5):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |            ")

    if (erros == 6):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      /     ")

    if (erros == 7):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      / \   ")

    print(" |            ")
    print("_|___         ")
    print()


def jogar(palavra_secreta,letras_acertadas,enforcou,acertou, erros, letras_faltando ):

    print(letras_acertadas)
    try:
        while (not acertou and not enforcou):
            chute = pede_chute()

            if (chute in palavra_secreta):
                marca_chute_correto(chute, letras_acertadas, palavra_secreta)
                letras_faltando = str(letras_acertadas.count('_'))
                if (letras_faltando == "0"):
                    print(
                        "PARABÉNS!! Você encontrou todas as letras formando a palavra '{}'".format(palavra_secreta.upper()))
            else:
                erros += 1
                print(letras_acertadas)
                print('Ainda faltam acertar {} letras'.format(letras_faltando))
                print('Você ainda tem {} tentativas'.format(7 - erros))
                desenha_forca(erros)

            # enforcou = erros == 7
            # acertou = "_" not in letras_acertadas
            if erros == 7:
                enforcou = True
            if '_' not in letras_acertadas:
                acertou = True

            print(letras_acertadas)

        if acertou:
            imprime_mensagem_vencedor()
            salva_ficheiro(palavra_secreta, letras_acertadas, enforcou, acertou, erros, letras_faltando,
                           interruptor=True)
        else:
            imprime_mensagem_perdedor(palavra_secreta)
            salva_ficheiro(palavra_secreta, letras_acertadas, enforcou, acertou, erros, letras_faltando,
                           interruptor=False)
        print("Fim do jogo")
    except KeyboardInterrupt as e:
        print("Jogo Interrompido", e)
        salva_ficheiro(palavra_secreta, letras_acertadas, enforcou, acertou, erros, letras_faltando, interruptor=False)
        exit()


def salva_ficheiro(palavra_secreta,letras_acertadas,enforcou,acertou, erros, letras_faltando, interruptor=False):
    load = carrega_ficheiro()
    if interruptor == False:
        load["FORCA"]["PALAVRA_SECRETA"] = palavra_secreta
        load["FORCA"]["LETRAS_ACERTADAS"] = letras_acertadas
        load["FORCA"]["ENFORCOU"] = enforcou
        load["FORCA"]["ACERTOU"] = acertou
        load["FORCA"]["ERROS"] = erros
        load["FORCA"]["LETRAS_FALTANDO"] = letras_faltando

    else:
        load["FORCA"]["PALAVRA_SECRETA"] = ""
        load["FORCA"]["LETRAS_ACERTADAS"] = ""
        load["FORCA"]["ENFORCOU"] = False
        load["FORCA"]["ACERTOU"] = False
        load["FORCA"]["ERROS"] = 0
        load["FORCA"]["LETRAS_FALTANDO"] = 0
        load["FORCA"]["VITORIAS"] += 1

    with open('saveforca.json', 'w') as jsonfile:
        json.dump(load, jsonfile)

def carrega_ficheiro():
    with open('saveforca.json', 'r') as jsonfile:
        load = json.load(jsonfile)
    return load

def main():
    while True:
        continuar = input("##########################\n0. Sair \n1. Jogar\nEscolha a opção:")
        load = carrega_ficheiro()
        if continuar == "1":
            option = input('\nNovo Jogo?[1]\nContinuar Antigo?[2]')
            imprime_mensagem_abertura()

            if option == '1':
                palavra_secreta = carrega_palavra_secreta()
                letras_acertadas = inicializa_letras_acertadas(palavra_secreta)
                enforcou = False
                acertou = False
                erros = 0
                letras_faltando = len(letras_acertadas)
                jogar(palavra_secreta,letras_acertadas,enforcou,acertou, erros, letras_faltando )
                continue

            elif option == '2':
                palavra_secreta = load["FORCA"]["PALAVRA_SECRETA"]
                if palavra_secreta == "":
                    print("Nao tem nenhum jogo salvo. Comecando novo jogo: ")
                    palavra_secreta = carrega_palavra_secreta()
                    letras_acertadas = inicializa_letras_acertadas(palavra_secreta)
                    enforcou = False
                    acertou = False
                    erros = 0
                    letras_faltando = len(letras_acertadas)
                    jogar(palavra_secreta, letras_acertadas, enforcou, acertou, erros, letras_faltando)
                else:
                    letras_acertadas = load["FORCA"]["LETRAS_ACERTADAS"]
                    enforcou = load["FORCA"]["ENFORCOU"]
                    acertou = load["FORCA"]["ACERTOU"]
                    erros = load["FORCA"]["ERROS"]
                    letras_faltando = load["FORCA"]["LETRAS_FALTANDO"]

                    jogar(palavra_secreta, letras_acertadas, enforcou, acertou, erros, letras_faltando)
                    continue
        elif continuar == "0":
            print("Saiu do jogo")
            exit()
        else:
            print("Opção inválida, tente outra vez.")
            continue


if (__name__ == '__main__'):
    main()
