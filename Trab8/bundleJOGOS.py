import exercicioCAMPOMINAS
from Trab6 import exercicioGLORIA
from Trab8 import exercicioVELHA, exercicio4LINHA, exercicioFORCA, exercicioBATALHANAVAL

#Teste do GIT para testar conexao

def menu_jogos():
    while True:
        print("""       
                      ______-----*    BUNDLE DE JOGOS    *-----______\n\n
                      
                                    1- JOGO 4 EM LINHA \n
                                    2- JOGO DA GLORIA \n
                                    3- JOGO DA FORCA \n
                                    4- JOGO CAMPO DE MINAS \n
                                    5- JOGO DA VELHA \n
                                    6- JOGO DA BATALHA NAVAL
                                    
\n**Pode parar o jogo a qualquer momento premindo CTRL-C e Enter de seguida""")
        escolha = str(input('\nQual jogo quer jogar?'))
        if escolha == '1':
            print('\n*JOGO 4 EM LINHA*\n')
            exercicio4LINHA.main()
        elif escolha == '2':
            print('\n*JOGO DA GLORIA*\n')
            exercicioGLORIA.game()
        elif escolha == '3':
            print('\n*JOGO DA FORCA*\n')
            exercicioFORCA.main()
        elif escolha == '4':
            print('\n*JOGO CAMPO DE MINA*\n')
            exercicioCAMPOMINAS.main()
        elif escolha == '5':
            print('\n*JOGO DA VELHA*\n')
            exercicioVELHA.main()
        elif escolha == '6':
            print('\n*JOGO DA BATALHA NAVAL*\n')
            exercicioBATALHANAVAL.main()
        else:
            print("Opção não válida!")
            continue

menu_jogos()