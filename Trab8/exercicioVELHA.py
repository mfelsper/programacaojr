from random import randint
import json
import keyboard

def ValorAleatorio():
    return randint(1, 3)


def limpaTabuleiro(board):
    for linha in range(len(board)):
        for coluna in range(3):
            board[linha][coluna] = 0


def valida(board):
    #  validar linhas
    for i in range(3):
        soma = board[i][0] + board[i][1] + board[i][2]
        if soma == 3 or soma == -3:
            return 1

    # validar colunas
    for i in range(3):
        soma = board[0][i] + board[1][i] + board[2][i]
        if soma == 3 or soma == -3:
            return 1

    # validar diagonais
    diagonal1 = board[0][0] + board[1][1] + board[2][2]
    diagonal2 = board[0][2] + board[1][1] + board[2][0]
    if diagonal1 == 3 or diagonal1 == -3 or diagonal2 == 3 or diagonal2 == -3:
        return 1

    return 0


def desenhar(board):
    for i in range(3):
        for j in range(3):
            if board[i][j] == 0:
                print(" _ ", end=' ')
            elif board[i][j] == 1:
                print(" X ", end=' ')
            elif board[i][j] == -1:
                print(" O ", end=' ')

        print()


def main():
    while True:
        continuar = input("##########################\n0. Sair \n1. SinglePlayer\n2. MultiPlayer\nEscolha a opção:")
        savegalo = carrega_ficheiro()
        if continuar == "1":
            option=input('\nNovo Jogo?[1]\nContinuar Antigo?[2]')
            
            if option == '1':
                jogoSingle(board=[[0,0,0],[0,0,0],[0,0,0]], jogada=0)
                continue
            elif option == '2':
                
                board = savegalo["GALO"]["SINGLEPLAYER"]["board"]
                jogada = savegalo["GALO"]["SINGLEPLAYER"]["jogada"]
                jogoSingle(board, jogada)
                print()
                continue
        if continuar == "2":
            option = input('\nNovo Jogo?[1]\nContinuar Antigo?[2]')
            if option == '1':
                jogoMulti(board=[[0, 0, 0], [0, 0, 0], [0, 0, 0]], jogada=0)
                continue
            elif option == '2':
                board = savegalo["GALO"]["MULTIPLAYER"]["board"]
                jogada = savegalo["GALO"]["MULTIPLAYER"]["jogada"]
                jogoMulti(board, jogada)
                print()
                continue
        elif continuar == "0":
            print("Saiu do jogo")
            exit()
        else:
            print("Opção inválida, tente outra vez.")
            continue


def carrega_ficheiro():
    with open('savegalo.json', 'r') as jsonfile:
        load = json.load(jsonfile)
    return load


def salva_ficheiro(modo_jogo, board, jogada, interruptor=False):
    load = carrega_ficheiro()
    load["GALO"][modo_jogo]["board"] = board

    if not interruptor: #interruptor == False:
        load["GALO"][modo_jogo]["jogada"] = 0
        if jogada % 2 + 1 == 1:
            load["GALO"][modo_jogo]["VITORIAS"]["JOGADOR1"] += 1
        else:
            load["GALO"][modo_jogo]["VITORIAS"]["JOGADOR2"] += 1
    else:
        load["GALO"][modo_jogo]["jogada"] = jogada
    with open('savegalo.json', 'w') as jsonfile:
        json.dump(load, jsonfile)

def jogoMulti(board, jogada):
    modo_jogo = "MULTIPLAYER" 
    try:
        while valida(board) == 0:
            print("\n##########################\nJogador ", jogada % 2 + 1)
            desenhar(board)
            lista_linhas_colunas_possiveis = ["1", "2", "3"]
            linha = input("\nLinha :")
            # if linha == "G":
            #     raise KeyboardInterrupt
            coluna = input("Coluna:")
            # if coluna == "G":
            #     raise KeyboardInterrupt

            if linha in lista_linhas_colunas_possiveis and coluna in lista_linhas_colunas_possiveis:
                if board[int(linha) - 1][int(coluna) - 1] == 0:
                    if jogada % 2 + 1 == 1:
                        board[int(linha) - 1][int(coluna) - 1] = 1
                    else:
                        board[int(linha) - 1][int(coluna) - 1] = -1
                else:
                    print('Opcao invalida')
                    jogada -= 1
            else:
                print("Posição inválida!")
                jogada -= 1
            if valida(board) == 1:
                limpaTabuleiro(board)
                salva_ficheiro(modo_jogo, board, jogada)
                print("Jogador ", jogada % 2 + 1, " ganhou após ", jogada + 1, " jogadas")
                break
            jogada += 1
            if jogada >= 9:
                limpaTabuleiro(board)
                if valida(board) == 1:
                    salva_ficheiro(modo_jogo, board, 0)
                    print("Jogador ", jogada % 2 + 1, " ganhou após ", jogada + 1, " jogadas. NA ULTIMA RODADA")
                    break
                else:
                    salva_ficheiro(modo_jogo, board, 0, True)
                    print('empate')
                    break
    except KeyboardInterrupt:
        salva_ficheiro(modo_jogo, board, jogada, True)
        exit()


def jogoSingle(board, jogada):
    modo_jogo = "SINGLEPLAYER"
    try:
        while valida(board) == 0:
            print("\n##########################\nJogador ", jogada % 2 + 1)
            desenhar(board)
            lista_linhas_colunas_possiveis = [1, 2, 3]
            if jogada % 2 + 1 == 1:
                linha = input("\nLinha :")
                if linha == 'G':
                    raise KeyboardInterrupt
                coluna = input("Coluna:")
                if coluna == 'G':
                    raise KeyboardInterrupt
                coluna = int(coluna)
                linha = int(linha)
                if linha in lista_linhas_colunas_possiveis and coluna in lista_linhas_colunas_possiveis:
                    if board[linha - 1][coluna - 1] == 0:
                        board[linha - 1][coluna - 1] = 1
                        if valida(board) == 1:
                            limpaTabuleiro(board)
                            salva_ficheiro(modo_jogo,board, jogada)
                            desenhar(board)
                            print("Jogador ", jogada % 2 + 1, " ganhou após ", jogada + 1, " jogadas")
                            break
                    else:
                        print('Casa Ocupada')
                        jogada -= 1
                else:
                    print("Posição inválida!")
                    jogada -= 1
            else:
                linha = ValorAleatorio()
                coluna = ValorAleatorio()
                if linha in lista_linhas_colunas_possiveis and coluna in lista_linhas_colunas_possiveis:
                    if board[linha - 1][coluna - 1] == 0:
                        board[linha - 1][coluna - 1] = -1
                        if valida(board) == 1:
                            limpaTabuleiro(board)
                            salva_ficheiro(modo_jogo,board, jogada)
                            desenhar(board)
                            print("Jogador ", jogada % 2 + 1, " ganhou após ", jogada + 1, " jogadas")
                            break
                    else:
                        jogada -= 1
                else:
                    jogada -= 1
            jogada += 1
            if jogada >= 9:
                if valida(board) == 1:
                    desenhar(board)
                    print("Jogador ", jogada % 2 + 1, " ganhou após ", jogada + 1, " jogadas. NA ULTIMA RODADA")
                    limpaTabuleiro(board)
                    salva_ficheiro(modo_jogo, board, jogada)
                    break
                else:
                    desenhar(board)
                    print('empate')
                    limpaTabuleiro(board)
                    salva_ficheiro(modo_jogo, board, 0, True)
                    break
    except KeyboardInterrupt:
        salva_ficheiro(modo_jogo, board, jogada, True)
        exit()