import json
import random


def carrega_ficheiro():
    with open('save4emlinha.json', 'r') as jsonfile:
        load = json.load(jsonfile)
    return load


def salva_ficheiro(modo_jogo, board, jogada, interruptor=False):
    load = carrega_ficheiro()
    load["4EMLINHA"][modo_jogo]["board"] = board

    if not interruptor: #interruptor == False:
        load["4EMLINHA"][modo_jogo]["jogada"] = 0
        if jogada % 2 + 1 == 1:
            load["4EMLINHA"][modo_jogo]["VITORIAS"]["JOGADOR1"] += 1
        else:
            load["4EMLINHA"][modo_jogo]["VITORIAS"]["JOGADOR2"] += 1
    else:
        load["4EMLINHA"][modo_jogo]["jogada"] = jogada
    with open('save4emlinha.json', 'w') as jsonfile:
        json.dump(load, jsonfile)


def ini_matriz(tamanho):
    # O unico propósito dessa funcao e iniciar uma matriz

    m = [[0 for g in range(tamanho[1])] for i in range(tamanho[0])]  # tamanho e um tuple

    return m, 1


def mostra_matriz(matriz):
    # essa funcao serve pra printar a matriz
    for linha in matriz:
        for i in range(len(linha)):
            if linha[i] == 0:
                linha[i] = "_"
            elif linha[i] == 1:
                linha[i] = "X"
            elif linha[i] == -1:
                linha[i] = "O"
        print(linha)
        for i in range(len(linha)):
            if linha[i] == "_":
                linha[i] = 0
            elif linha[i] == "O":
                linha[i] = -1
            elif linha[i] == "X":
                linha[i] = 1


def jogada(matriz, coluna, token):
    for i in range(len(matriz[0]) - 1, -1, -1):
        if matriz[i][coluna] == 0:
            Temp = matriz[i][coluna]
            matriz[i][coluna] = token
            token = Temp
            # token, matriz[i][coluna] = matriz[i][coluna], token
            # print(matriz[i][coluna])
            # print(token, type(token))
        elif matriz[i][coluna] == '_':
            Temp = matriz[i][coluna]
            matriz[i][coluna] = token
            token = Temp
        elif i == 0:
            if matriz[0][coluna] != 0 or matriz[0][coluna] != '_':
                return matriz, 0
        else:
            continue
    return matriz, 1


def verificar(jogo):
    # essa funcao verifica se há ganhadores
    # checando linhas
    for i in range(0, 6):
        for j in range(0, 3):
            soma = jogo[i][j] + jogo[i][j + 1] + jogo[i][j + 2] + jogo[i][j + 3]
            if soma == 4 or soma == -4:
                return 1
    # checando colunas
    for j in range(6):
        for i in range(3):
            soma = jogo[i][j] + jogo[i + 1][j] + jogo[i + 2][j] + jogo[i + 3][j]
            if soma == 4 or soma == -4:
                return 1
    # checando diagonais
    for j in range(2):
        diagonal1 = jogo[j][j] + jogo[j + 1][j + 1] + jogo[j + 2][j + 2] + jogo[j + 3][j + 3]
        diagonal2 = jogo[5 - j][j] + jogo[4 - j][j + 1] + jogo[3 - j][j + 2] + jogo[2 - j][j + 3]
        if diagonal1 == 4 or diagonal2 == 4 or diagonal1 == -4 or diagonal2 == -4:
            return 1
    return 0


def multiPlayerGame(jogo, jogador):
    modo_jogo = "MULTIPLAYER"

    try:
        while True:

            mostra_matriz(jogo[0])
            print('\n')
            # mostra_matriz(jogo)
            print('\n')
            print('Quem vai jogar é o jogador ', jogador % 2 + 1, '.')

            print('Escolha em qual coluna você quer jogar (escolha de 1 a 6): ')

            try:
                escolha = int(input())
                while escolha < 1 or escolha > 6:
                    print('Número inválido.')

                    print('Escolha em qual coluna você quer jogar (escolha de 1 a 6 ): ')

                    escolha = int(input())

                if jogador % 2 + 1 == 1:
                    token = 1
                    token_2 = 'X'
                else:
                    token = -1
                    token_2 = 'O'

                jogo = jogada(jogo[0], escolha - 1, token)
                if jogo[1] == 0:
                    print("Coluna cheia! ")
                    jogador -= 1

                if verificar(jogo[0]):
                    salva_ficheiro(modo_jogo, jogo[0], jogador)
                    print("Jogador ", jogador % 2 + 1, " = ", token_2, " ganhou apos ", jogador + 1, " rodadas")
                    mostra_matriz(jogo[0])
                    print('\n')
                    break
                elif jogador == 36:
                    salva_ficheiro(modo_jogo, jogo[0], jogador, True)
                    print("Empate")
                    break
                jogador += 1
            except ValueError as vl:
                print(vl)
    except KeyboardInterrupt as e:
        print("Jogo interrompido", e)
        salva_ficheiro(modo_jogo, jogo[0], jogador, True)
        exit()



def singlePlayerGame(jogo, jogador):
    modo_jogo = "SINGLEPLAYER"
    try:
        while True:
            escolha = 0
            # print(jogo)
            mostra_matriz(jogo[0])
            print()
            # print(jogo)
            while escolha < 1 or escolha > 6:
                try:
                    if jogador % 2 + 1 == 1:
                        print(f"Quem vai jogar e o jogador 1")
                        escolha = int(input(f'Escolha em qual coluna você quer jogar (escolha de 1 a 6): '))
                    elif jogador % 2 + 1 != 1:
                        print(f"Quem vai jogar e o robo")
                        escolha = random.randint(1, 6)
                        print(f'Escolha em qual coluna você quer jogar (escolha de 1 a 6): {escolha} ')
                    else:
                        print("Opção inválida")
                except ValueError as vl:
                    print(vl)

            if jogador % 2 + 1 == 1:
                token = 1
                token_2 = 'X'
            else:
                token = -1
                token_2 = 'O'

            jogo = jogada(jogo[0], escolha - 1, token)
            if jogo[1] == 0:
                print('Coluna cheia!')
                jogador -= 1

            if verificar(jogo[0]):
                salva_ficheiro(modo_jogo, jogo[0], jogador)
                print("Jogador ", jogador % 2 + 1, " = ", token_2, " ganhou apos ", jogador + 1, " rodadas")
                mostra_matriz(jogo[0])
                print('\n')
                break
            elif jogador == 36:
                salva_ficheiro(modo_jogo, jogo[0], jogador, True)
                print("Empate")
                break
            jogador += 1
    except KeyboardInterrupt as e:
        print("Jogo Interrompido", e)
        salva_ficheiro(modo_jogo, jogo[0], jogador, True)
        exit()


def main():
    while True:
        continuar = input("##########################\n0. Sair \n1. SinglePlayer\n2. MultiPlayer\nEscolha a opção:")
        load = carrega_ficheiro()
        if continuar == "1":
            option = input('\nNovo Jogo?[1]\nContinuar Antigo?[2]')

            if option == '1':
                jogo = ini_matriz((6, 6))
                singlePlayerGame(jogo, 0)
                continue

            elif option == '2':
                jogo = (load["4EMLINHA"]["SINGLEPLAYER"]["board"], 1)
                jogada = load["4EMLINHA"]["SINGLEPLAYER"]["jogada"]
                singlePlayerGame(jogo, jogada)
                print()
                continue

        if continuar == "2":
            option = input('\nNovo Jogo?[1]\nContinuar Antigo?[2]')
            if option == '1':
                jogo = ini_matriz((6, 6))
                multiPlayerGame(jogo, 0)
                continue
            elif option == '2':
                jogo = (load["4EMLINHA"]["MULTIPLAYER"]["board"], 1)
                jogada = load["4EMLINHA"]["MULTIPLAYER"]["jogada"]
                multiPlayerGame(jogo, jogada)
                print()
                continue
        elif continuar == "0":
            print("Saiu do jogo")
            exit()
        else:
            print("Opção inválida, tente outra vez.")
            continue


if __name__ == "__main__":
    main()
