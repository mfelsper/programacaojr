from itertools import permutations
from random import randint


def jogar_dado():
    posicoes_para_andar = (randint(1, 6))
    return posicoes_para_andar


def concatenacao(listajogadores):
    resultado = set()

    for i in range(1, len(listajogadores) + 1):
        resultado.update("".join(r) for r in permutations(listajogadores, i))

    return list(resultado)


# def concatenacao(listajogadores):
#     lista = []
#     b = "(), '"
#     for L in range(0, len(listajogadores) + 1):
#         for subset in itertools.combinations(listajogadores, L):
#             a = f'{subset}'
#             lista.append(a)
#     for j in range(len(lista)):
#         for i in range(len(b)):
#             lista[j] = lista[j].replace(b[i], '')
#     del lista[0]
#
#
#     return lista


def inicia_tabuleiro():
    tabuleiro_vis = []
    for i in range(52):
        tabuleiro_vis.append(0)
    tabuleiro_vis[3] = '+3'
    tabuleiro_vis[5] = 'zero'  # volta para inicio
    tabuleiro_vis[8] = '+3'
    tabuleiro_vis[16] = 'j-1'
    tabuleiro_vis[22] = '+2'
    tabuleiro_vis[27] = 'jj'  # junta jogador
    tabuleiro_vis[31] = '+1'
    tabuleiro_vis[35] = 'j-1'  # jogador -1
    tabuleiro_vis[38] = '+4'
    tabuleiro_vis[44] = '-1'
    tabuleiro_vis[49] = '-1'
    tabuleiro_vis[51] = 'CHEGADA'
    return tabuleiro_vis


def define_jogadores(numero_jogadores):
    lista_jogadores = []
    for jogador in range(numero_jogadores):
        jogador = input('Nome Jogador? Coloque na Ordem que iram jogar').upper()
        lista_jogadores.append(jogador)

    return lista_jogadores


def mostrar(tabuleiro):
    metade_tabuleiro = int(len(tabuleiro) / 2)
    # print(metade_tabuleiro)
    mostrar2 = tabuleiro[0:metade_tabuleiro]
    mostrar3 = tabuleiro[metade_tabuleiro:len(tabuleiro)]
    totalmostrar = f'*|INICIO|* {mostrar2}\n           ☇⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚⇚↩\n           {mostrar3} '
    return totalmostrar


def transforma_jogador_numero(lista_de_jogadores):
    lista_de_jogadores_numero = []
    for i in range(len(lista_de_jogadores)):
        lista_de_jogadores_numero.append(i)
    return lista_de_jogadores_numero


def game():

    # Comeco criando uma lista com os jogadores que iram particapar

    # jogadores = int(input('Quantos jogadores iram participar? '))

    lista_jogadores = ['Marcos', 'Felipe', 'Ramos', 'Luis']
    lista_jogadores_pos = []
    contador_remover = []
    for i in range(len(lista_jogadores)):
        lista_jogadores_pos.append(0)
        contador_remover.append(0)

    lista_posicoes_especiais = ['+4', '+3', '+2', '+1', 'zero', 'j-1', 'jj', '-1', 'CHEGADA']

    # Inicio os tabuleiros
    tabuleiro_pos = inicia_tabuleiro()
    lista_concatenacao = concatenacao(lista_jogadores)  # +concatenacao(lista_jogadores_reversa)
    print(lista_concatenacao)
    print(f'''\n
                                                --------------------
                                                |**JOGO DA GLORIA**|
                                                --------------------
                                Bem vindo jogadores: {lista_jogadores}. Boa sorte!\n
                                *| INICIO = Posição incial de todos os jogadores |*\n
''')
    while True:
        for i in range(len(lista_jogadores)):
            print('\n', mostrar(tabuleiro_pos))
            print('\n\n_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-')
            print('Jogador', lista_jogadores[i], 'lance o dado: (para lançar o dado prima X) ')

            while True:
                x = input().upper()
                if x == 'X':
                    # casas_a_andar = jogar_dado()
                    casas_a_andar = int(input('casa?'))
                    break
                else:
                    print('Para lançar o dado prima X')

            dado = f'🎲 = {casas_a_andar + 1}'

            print(dado)
            print('Jogador', lista_jogadores[i], 'estava na posição', lista_jogadores_pos[i], '.')

            posicao_a_andar = lista_jogadores_pos[i] + casas_a_andar
            print(posicao_a_andar)

            # condicao para ganhar
            if posicao_a_andar > 51:

                print(f'''PARABÉNS!!!\nO Jogador, {lista_jogadores[i]} ganhou o jogo!!!''')
                print("       ___________      ")
                print("      '._==_==_=_.'     ")
                print("      .-\\:      /-.    ")
                print("     | (|:.     |) |    ")
                print("      '-|:.     |-'     ")
                print("        \\::.    /      ")
                print("         '::. .'        ")
                print("           ) (          ")
                print("         _.' '._        ")
                print("        '-------'       ")
                return 0
            else:
                # condicoes especiais
                if tabuleiro_pos[posicao_a_andar] in lista_posicoes_especiais:

                    if tabuleiro_pos[posicao_a_andar] == 'zero':
                        print(f'''                    |--------------------------------------------------------------------------------------------|
                                Jogador', {lista_jogadores[i]}, 'caiu em uma posição especial:     |=> "zero" <=|                                               
                                                  Terá que retornar para o Ínicio                                
                    |--------------------------------------------------------------------------------------------|''')
                        if lista_jogadores[i] not in tabuleiro_pos:  # se na primeira jogada cair em zero
                            lista_jogadores_pos[i] = 0
                            contador_remover[i] = 0
                        else:
                            ind = tabuleiro_pos.index(lista_jogadores[i])
                            tabuleiro_pos[ind] = 0
                            lista_jogadores_pos[i] = 0
                            contador_remover[i] = 0
                    elif tabuleiro_pos[posicao_a_andar] == '+4':
                        print(f'''                                    |---------------------------------------------------------------------------------------|
                                                    Jogador, {lista_jogadores[i]}, caiu em uma posição especial |=> "+4" <=|              
                                     Irá andar 4 casa além das {(casas_a_andar + 1)}, que tinha direito! Ou seja andará um total de:     {(casas_a_andar + 1) + 4}    
                                    |---------------------------------------------------------------------------------------|''')
                        if tabuleiro_pos[posicao_a_andar + 4] == 0 and tabuleiro_pos[posicao_a_andar - casas_a_andar] == \
                                lista_jogadores[i] \
                                or tabuleiro_pos[posicao_a_andar + 4] == 0 and tabuleiro_pos[
                            posicao_a_andar - casas_a_andar] == 0:
                            tabuleiro_pos[posicao_a_andar + 4] = lista_jogadores[i]
                            contador_remover[i] = lista_jogadores_pos[i]
                            tabuleiro_pos[contador_remover[i]] = 0
                            lista_jogadores_pos[i] = posicao_a_andar + 4
                            print('Dessa forma irá andar até a posição', lista_jogadores_pos[i] + 1)
                        else:
                            for concac in lista_concatenacao:
                                if tabuleiro_pos[
                                    posicao_a_andar + 4] == concac and posicao_a_andar - casas_a_andar == 0:
                                    jogadortwo = concac
                                    jogador = lista_jogadores[i]  # jogador que esta jogando
                                    tabuleiro_pos[posicao_a_andar + 4] = jogadortwo + jogador
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = 0
                                    lista_jogadores_pos[i] = posicao_a_andar + 4
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                    break
                                elif tabuleiro_pos[posicao_a_andar + 4] == concac \
                                        and tabuleiro_pos[posicao_a_andar - casas_a_andar] == lista_jogadores[i]:
                                    jogadortwo = concac
                                    jogador = lista_jogadores[i]  # jogador que esta jogando
                                    tabuleiro_pos[posicao_a_andar + 4] = jogadortwo + jogador
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = 0
                                    lista_jogadores_pos[i] = posicao_a_andar + 4
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                    break
                                elif tabuleiro_pos[posicao_a_andar + 4] == 0 and \
                                        tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                    jogadortwo = concac
                                    tabuleiro_pos[posicao_a_andar + 4] = lista_jogadores[i]
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = jogadortwo.replace(lista_jogadores[i], '')
                                    lista_jogadores_pos[i] = posicao_a_andar + 4
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    break
                                elif tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                    jogador = tabuleiro_pos[posicao_a_andar + 4]
                                    print(tabuleiro_pos[posicao_a_andar])
                                    tabuleiro_pos[posicao_a_andar + 4] = jogador + lista_jogadores[i]
                                    print('terceiro caso')
                                    tabuleiro_pos[posicao_a_andar - casas_a_andar] = concac.replace(lista_jogadores[i],
                                                                                                    '')
                                    # tabuleiro_pos[posicao_a_andar] = tabuleiro_pos[posicao_a_andar] + lista_jogadores[i]
                                    print(tabuleiro_pos[posicao_a_andar - casas_a_andar])
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    lista_jogadores_pos[i] = posicao_a_andar + 4
                                    break
                    elif tabuleiro_pos[posicao_a_andar] == '+2':
                        print(f'''                                    |---------------------------------------------------------------------------------------|
                                                    Jogador, {lista_jogadores[i]}, caiu em uma posição especial |=> "+2" <=|              
                                     Irá andar 2 casas além das {(casas_a_andar + 1)}, que tinha direito! Ou seja andará um total de:     {(casas_a_andar + 1) + 2}    
                                    |---------------------------------------------------------------------------------------|''')
                        if tabuleiro_pos[posicao_a_andar + 2] == 0 and tabuleiro_pos[posicao_a_andar - casas_a_andar] == \
                                lista_jogadores[i] \
                                or tabuleiro_pos[posicao_a_andar + 2] == 0 and tabuleiro_pos[
                            posicao_a_andar - casas_a_andar] == 0:
                            tabuleiro_pos[posicao_a_andar + 2] = lista_jogadores[i]
                            contador_remover[i] = lista_jogadores_pos[i]
                            tabuleiro_pos[contador_remover[i]] = 0
                            lista_jogadores_pos[i] = posicao_a_andar + 2
                            print('Dessa forma irá andar até a posição', lista_jogadores_pos[i] + 1)
                        else:
                            for concac in lista_concatenacao:
                                if tabuleiro_pos[
                                    posicao_a_andar + 2] == concac and posicao_a_andar - casas_a_andar == 0:
                                    jogadortwo = concac
                                    jogador = lista_jogadores[i]  # jogador que esta jogando
                                    tabuleiro_pos[posicao_a_andar + 2] = jogadortwo + jogador
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = 0
                                    lista_jogadores_pos[i] = posicao_a_andar + 2
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                    break
                                elif tabuleiro_pos[posicao_a_andar + 2] == concac \
                                        and tabuleiro_pos[posicao_a_andar - casas_a_andar] == lista_jogadores[i]:
                                    jogadortwo = concac
                                    jogador = lista_jogadores[i]  # jogador que esta jogando
                                    tabuleiro_pos[posicao_a_andar + 2] = jogadortwo + jogador
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = 0
                                    lista_jogadores_pos[i] = posicao_a_andar + 2
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                    break
                                elif tabuleiro_pos[posicao_a_andar + 2] == 0 and \
                                        tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                    jogadortwo = concac
                                    tabuleiro_pos[posicao_a_andar + 2] = lista_jogadores[i]
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = jogadortwo.replace(lista_jogadores[i], '')
                                    lista_jogadores_pos[i] = posicao_a_andar + 2
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    break
                                elif tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                    jogador = tabuleiro_pos[posicao_a_andar + 2]
                                    print(tabuleiro_pos[posicao_a_andar])
                                    tabuleiro_pos[posicao_a_andar + 2] = jogador + lista_jogadores[i]
                                    print('terceiro caso')
                                    tabuleiro_pos[posicao_a_andar - casas_a_andar] = concac.replace(lista_jogadores[i],
                                                                                                    '')
                                    # tabuleiro_pos[posicao_a_andar] = tabuleiro_pos[posicao_a_andar] + lista_jogadores[i]
                                    print(tabuleiro_pos[posicao_a_andar - casas_a_andar])
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    lista_jogadores_pos[i] = posicao_a_andar + 2
                                    break
                    elif tabuleiro_pos[posicao_a_andar] == '+1':
                        print(f'''                           |---------------------------------------------------------------------------------------|
                                           Jogador, {lista_jogadores[i]}, caiu em uma posição especial |=> "+1" <=|              
                            Irá andar 1 casa além das {(casas_a_andar + 1)}, que tinha direito! Ou seja andará um total de:     {(casas_a_andar + 1) + 1}    
                           |---------------------------------------------------------------------------------------|''')
                        if tabuleiro_pos[posicao_a_andar + 1] == 0 and tabuleiro_pos[posicao_a_andar - casas_a_andar] == \
                                lista_jogadores[i] \
                                or tabuleiro_pos[posicao_a_andar + 1] == 0 and tabuleiro_pos[
                            posicao_a_andar - casas_a_andar] == 0:
                            tabuleiro_pos[posicao_a_andar + 1] = lista_jogadores[i]
                            contador_remover[i] = lista_jogadores_pos[i]
                            tabuleiro_pos[contador_remover[i]] = 0
                            lista_jogadores_pos[i] = posicao_a_andar + 1
                            print('Dessa forma irá andar até a posição', lista_jogadores_pos[i] + 1)
                        else:
                            for concac in lista_concatenacao:
                                if tabuleiro_pos[
                                    posicao_a_andar + 1] == concac and posicao_a_andar - casas_a_andar == 0:
                                    jogadortwo = concac
                                    jogador = lista_jogadores[i]  # jogador que esta jogando
                                    tabuleiro_pos[posicao_a_andar + 1] = jogadortwo + jogador
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = 0
                                    lista_jogadores_pos[i] = posicao_a_andar + 1
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                    break
                                elif tabuleiro_pos[posicao_a_andar + 1] == concac \
                                        and tabuleiro_pos[posicao_a_andar - casas_a_andar] == lista_jogadores[i]:
                                    jogadortwo = concac
                                    jogador = lista_jogadores[i]  # jogador que esta jogando
                                    tabuleiro_pos[posicao_a_andar + 1] = jogadortwo + jogador
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = 0
                                    lista_jogadores_pos[i] = posicao_a_andar + 1
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                    break
                                elif tabuleiro_pos[posicao_a_andar + 1] == 0 and \
                                        tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                    jogadortwo = concac
                                    tabuleiro_pos[posicao_a_andar + 1] = lista_jogadores[i]
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = jogadortwo.replace(lista_jogadores[i], '')
                                    lista_jogadores_pos[i] = posicao_a_andar + 1
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    break
                                elif tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                    jogador = tabuleiro_pos[posicao_a_andar + 1]
                                    print(tabuleiro_pos[posicao_a_andar])
                                    tabuleiro_pos[posicao_a_andar + 1] = jogador + lista_jogadores[i]
                                    print('terceiro caso')
                                    tabuleiro_pos[posicao_a_andar - casas_a_andar] = concac.replace(lista_jogadores[i],
                                                                                                    '')
                                    # tabuleiro_pos[posicao_a_andar] = tabuleiro_pos[posicao_a_andar] + lista_jogadores[i]
                                    print(tabuleiro_pos[posicao_a_andar - casas_a_andar])
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    lista_jogadores_pos[i] = posicao_a_andar + 1
                                    break
                    elif tabuleiro_pos[posicao_a_andar] == '-1':
                        print(f'''                                           |---------------------------------------------------------------------------------------|
                                                           Jogador, {lista_jogadores[i]}, caiu em uma posição especial |=> "-1" <=|              
                                                                Irá andar 1 casa para trás, pois calhou em -1!     
                                           |---------------------------------------------------------------------------------------|''')
                        if tabuleiro_pos[43] == 0 and tabuleiro_pos[43 - casas_a_andar] == lista_jogadores[i] \
                                or tabuleiro_pos[43] == 0 and tabuleiro_pos[43 - casas_a_andar] == 0:
                            tabuleiro_pos[posicao_a_andar - 1] = lista_jogadores[i]
                            contador_remover[i] = lista_jogadores_pos[i]
                            tabuleiro_pos[contador_remover[i]] = 0
                            lista_jogadores_pos[i] = 43
                            print('Dessa forma irá andar até a posição', lista_jogadores_pos[i] + 1)
                        else:
                            for concac in lista_concatenacao:
                                if tabuleiro_pos[43] == concac and 43 - casas_a_andar == 0:
                                    jogadortwo = concac
                                    jogador = lista_jogadores[i]  # jogador que esta jogando
                                    tabuleiro_pos[posicao_a_andar - 1] = jogadortwo + jogador
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = 0
                                    lista_jogadores_pos[i] = posicao_a_andar - 1
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                    break
                                elif tabuleiro_pos[43] == concac \
                                        and tabuleiro_pos[posicao_a_andar - casas_a_andar] == lista_jogadores[i]:
                                    jogadortwo = concac
                                    jogador = lista_jogadores[i]  # jogador que esta jogando
                                    tabuleiro_pos[posicao_a_andar - 1] = jogadortwo + jogador
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = 0
                                    lista_jogadores_pos[i] = posicao_a_andar - 1
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                    break
                                elif tabuleiro_pos[posicao_a_andar - 1] == 0 and \
                                        tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                    jogadortwo = concac
                                    tabuleiro_pos[posicao_a_andar - 1] = lista_jogadores[i]
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = jogadortwo.replace(lista_jogadores[i], '')
                                    lista_jogadores_pos[i] = posicao_a_andar - 1
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    break
                                elif tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                    jogador = tabuleiro_pos[posicao_a_andar - 1]
                                    print(tabuleiro_pos[posicao_a_andar])
                                    tabuleiro_pos[posicao_a_andar - 1] = jogador
                                    print('terceiro caso')
                                    tabuleiro_pos[posicao_a_andar - casas_a_andar] = concac.replace(lista_jogadores[i],
                                                                                                    '')
                                    # tabuleiro_pos[posicao_a_andar] = tabuleiro_pos[posicao_a_andar] + lista_jogadores[i]
                                    print(tabuleiro_pos[posicao_a_andar - casas_a_andar])
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    lista_jogadores_pos[i] = posicao_a_andar - 1
                                    break
                    elif tabuleiro_pos[posicao_a_andar] == 'j-1':

                        print(f'''                                    |---------------------------------------------------------------------------------------|
                                                    Jogador, {lista_jogadores[i]}, caiu em uma posição especial |=> "j-1" <=|              
                                                      NÃO JOGARÁ ESTA RODADA! Ou seja permanecerá na posição {lista_jogadores_pos[i]}
                                    |---------------------------------------------------------------------------------------|''')

                        pass
                    elif tabuleiro_pos[posicao_a_andar] == 'jj':
                        print(f'''                                |---------------------------------------------------------------------------------------|
                                                Jogador, {lista_jogadores[i]}, caiu em uma posição especial |=> "jj" <=|              
                                                        Verificando se há algum jogador a sua frente...
                                |---------------------------------------------------------------------------------------|\n''')
                        ind = lista_jogadores_pos[i]
                        for j in range(ind + 1, len(tabuleiro_pos)):
                            if tabuleiro_pos[j] in lista_concatenacao:
                                print(
                                    f'                          **| ENCONTRAMOS O JOGADOR {tabuleiro_pos[j]} Á SUA FRENTE! DESSA FORMA {lista_jogadores[i]} E {tabuleiro_pos[j]} SERÃO JUNTADOS |**')
                                for concac in lista_concatenacao:
                                    if tabuleiro_pos[lista_jogadores_pos[i]] == lista_jogadores[i]:
                                        tabuleiro_pos[lista_jogadores_pos[i]] = 0
                                        break
                                    elif tabuleiro_pos[lista_jogadores_pos[i]] == concac:
                                        tabuleiro_pos[lista_jogadores_pos[i]] = concac.replace(lista_jogadores[i], '')
                                        break
                                tabuleiro_pos[j] = tabuleiro_pos[j] + lista_jogadores[i]
                                lista_jogadores_pos[i] = j
                                break
                        else:
                            print(
                                '                                             **| NÃO FORAM ENCONTRADOS NENHUM JOGADOR A SUA FRENTE |**\n'
                                '                                  **| DESSA FORMA NAO SE JUNTARÁ A NENHUM JOGADOR E PERMANCERÁ NO MESMA CASA |**')
                    elif tabuleiro_pos[posicao_a_andar] == '+3':
                        print(f'''                            |---------------------------------------------------------------------------------------|
                                                Jogador, {lista_jogadores[i]}, caiu em uma posição especial |=> "+3" <=|              
                                   Irá andar 3 casas além das {(casas_a_andar + 1)}, que tinha direito! Ou seja andará um total de:     {(casas_a_andar + 1) + 3}    
                                |---------------------------------------------------------------------------------------|''')

                        if tabuleiro_pos[posicao_a_andar + 3] == 0 and tabuleiro_pos[posicao_a_andar - casas_a_andar] == \
                                lista_jogadores[i] \
                                or tabuleiro_pos[posicao_a_andar + 3] == 0 and tabuleiro_pos[
                            posicao_a_andar - casas_a_andar] == 0:
                            tabuleiro_pos[posicao_a_andar + 3] = lista_jogadores[i]
                            contador_remover[i] = lista_jogadores_pos[i]
                            tabuleiro_pos[contador_remover[i]] = 0
                            lista_jogadores_pos[i] = posicao_a_andar + 3
                            print('Dessa forma irá andar até a posição', lista_jogadores_pos[i] + 1)
                        else:
                            for concac in lista_concatenacao:
                                if tabuleiro_pos[
                                    posicao_a_andar + 3] == concac and posicao_a_andar - casas_a_andar == 0:
                                    jogadortwo = concac
                                    jogador = lista_jogadores[i]  # jogador que esta jogando
                                    tabuleiro_pos[posicao_a_andar + 3] = jogadortwo + jogador
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = 0
                                    lista_jogadores_pos[i] = posicao_a_andar + 3
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                    break
                                elif tabuleiro_pos[posicao_a_andar + 3] == concac \
                                        and tabuleiro_pos[posicao_a_andar - casas_a_andar] == lista_jogadores[i]:
                                    jogadortwo = concac
                                    jogador = lista_jogadores[i]  # jogador que esta jogando
                                    tabuleiro_pos[posicao_a_andar + 3] = jogadortwo + jogador
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = 0
                                    lista_jogadores_pos[i] = posicao_a_andar + 3
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                    break


                                elif tabuleiro_pos[posicao_a_andar + 3] == 0 and \
                                        tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                    jogadortwo = concac
                                    tabuleiro_pos[posicao_a_andar + 3] = lista_jogadores[i]
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    tabuleiro_pos[contador_remover[i]] = jogadortwo.replace(lista_jogadores[i], '')
                                    lista_jogadores_pos[i] = posicao_a_andar + 3
                                    print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                          lista_jogadores_pos[i] + 1, 'pois', dado)
                                    break

                                elif tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                    jogador = tabuleiro_pos[posicao_a_andar + 3]
                                    print(tabuleiro_pos[posicao_a_andar])
                                    tabuleiro_pos[posicao_a_andar + 3] = jogador + lista_jogadores[i]
                                    print('terceiro caso')
                                    tabuleiro_pos[posicao_a_andar - casas_a_andar] = concac.replace(lista_jogadores[i],
                                                                                                    '')
                                    # tabuleiro_pos[posicao_a_andar] = tabuleiro_pos[posicao_a_andar] + lista_jogadores[i]
                                    print(tabuleiro_pos[posicao_a_andar - casas_a_andar])
                                    contador_remover[i] = lista_jogadores_pos[i]
                                    lista_jogadores_pos[i] = posicao_a_andar + 3
                                    break

                # condição se for uma posição vazia
                else:
                    if tabuleiro_pos[posicao_a_andar] == 0 and tabuleiro_pos[posicao_a_andar - casas_a_andar] == \
                            lista_jogadores[i] \
                            or tabuleiro_pos[posicao_a_andar] == 0 and tabuleiro_pos[
                        posicao_a_andar - casas_a_andar] == 0:
                        print()
                        tabuleiro_pos[posicao_a_andar] = lista_jogadores[i]
                        contador_remover[i] = lista_jogadores_pos[i]
                        tabuleiro_pos[contador_remover[i]] = 0
                        lista_jogadores_pos[i] = posicao_a_andar
                        print('Dessa forma', lista_jogadores[i], 'irá andar até a posição', lista_jogadores_pos[i] + 1,
                              'pois', dado)
                    else:
                        for concac in lista_concatenacao:

                            if tabuleiro_pos[posicao_a_andar] == concac and posicao_a_andar - casas_a_andar == 0:
                                jogadortwo = concac
                                jogador = lista_jogadores[i]  # jogador que esta jogando
                                tabuleiro_pos[posicao_a_andar] = jogadortwo + jogador
                                contador_remover[i] = lista_jogadores_pos[i]
                                tabuleiro_pos[contador_remover[i]] = 0
                                lista_jogadores_pos[i] = posicao_a_andar
                                print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                      lista_jogadores_pos[i] + 1, 'pois', dado)
                                print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                break
                            elif tabuleiro_pos[posicao_a_andar] == concac \
                                    and tabuleiro_pos[posicao_a_andar - casas_a_andar] == lista_jogadores[i]:
                                jogadortwo = concac
                                jogador = lista_jogadores[i]  # jogador que esta jogando
                                tabuleiro_pos[posicao_a_andar] = jogadortwo + jogador
                                contador_remover[i] = lista_jogadores_pos[i]
                                tabuleiro_pos[contador_remover[i]] = 0
                                lista_jogadores_pos[i] = posicao_a_andar
                                print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                      lista_jogadores_pos[i] + 1, 'pois', dado)
                                print('ESTA CASA ESTA OCUPADA POR OUTRO JOGADOR. IRÃO DIVIDIR A CASA')
                                break

                            elif tabuleiro_pos[posicao_a_andar] == 0 and \
                                    tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                jogadortwo = concac
                                tabuleiro_pos[posicao_a_andar] = lista_jogadores[i]
                                contador_remover[i] = lista_jogadores_pos[i]
                                tabuleiro_pos[contador_remover[i]] = jogadortwo.replace(lista_jogadores[i], '')
                                lista_jogadores_pos[i] = posicao_a_andar
                                print('Dessa forma', lista_jogadores[i], 'irá andar até a posição',
                                      lista_jogadores_pos[i] + 1, 'pois', dado)
                                break

                            elif tabuleiro_pos[posicao_a_andar - casas_a_andar] == concac:
                                jogador = tabuleiro_pos[posicao_a_andar]
                                print(tabuleiro_pos[posicao_a_andar])
                                tabuleiro_pos[posicao_a_andar] = jogador + lista_jogadores[i]
                                print('terceiro caso')
                                tabuleiro_pos[posicao_a_andar - casas_a_andar] = concac.replace(lista_jogadores[i], '')
                                # tabuleiro_pos[posicao_a_andar] = tabuleiro_pos[posicao_a_andar] + lista_jogadores[i]
                                print(tabuleiro_pos[posicao_a_andar - casas_a_andar])
                                contador_remover[i] = lista_jogadores_pos[i]
                                lista_jogadores_pos[i] = posicao_a_andar
                                break



