def retornar_sigla():
    cadeiras_notas = {}
    cadeiras_notas_oficial = {}
    numero_cadeiras = int(input('Quantas cadeiras quer adicionar? '))
    for i in range(numero_cadeiras):
        cadeira = input('\nCadeira?')
        nota = input('Nota Pretendida?\n')
        cadeiras_notas[cadeira] = nota
    #print(cadeiras_notas)
    siglas = list(cadeiras_notas.keys())
    notas = list(cadeiras_notas.values())
    #print(siglas)
    for i in range(len(cadeiras_notas)):
        sigla = siglas[i].split()
        sigla_maiuscula = ''
        for j in range(len(sigla)):
            sigla_maiuscula = (sigla_maiuscula + sigla[j][0]).upper()

        cadeiras_notas_oficial[sigla_maiuscula] = notas[i]
    #print(cadeiras_notas_oficial)
    for i in cadeiras_notas_oficial:
        print(f'{i} tem nota minima de {cadeiras_notas_oficial[i]}')


retornar_sigla()