def potencia(base,expoente):
    resultado = 1
    for i in range (expoente):
        resultado = resultado * base
    return resultado

while True:
    op = input('\ncalcular? press ´s´\nTerminar? press anything else\n').upper()
    if op == 'S':
        base = int(input('Digite a base? '))
        expoente = int(input('Digite o expoente? '))
        print(f'{base} ^ {expoente} = {potencia(base, expoente)}')
    else:
        break