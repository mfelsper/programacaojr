def dez_valores():
    valores_totais = []
    for i in range (0,10):
        valor = int(input('Digite o valor a ser inserido! '))
        valores_totais.append(valor)
    valores_totais.sort()
    print(valores_totais)
    diferenca = valores_totais[9]-valores_totais[0]
    pares = []
    impares = []
    for numero in valores_totais:
        if numero%2==0:
            pares.append(numero)
        else:
            impares.append(numero)
    print(f'{valores_totais[9]} - {valores_totais[0]}={diferenca}')
    print(f'pares={pares}\nimpares={impares}')
dez_valores()